const request = require('supertest')
const app = require('../src/app')

describe('API test', () => {
    const mutationRequest = dna => request(app)
        .post('/api/mutation')
        .send({ dna: dna })

    const listDnaWithMutation = [
        ["ATGCGA", "CAGTGC", "TTATGT", "AGAAGG", "CCCCTA", "TCACTG"],
        ["ATGCGA", "CAGTCC", "TTATGT", "AGTAGG", "CTCCTA", "TCACTG"],
        ["ATGCGA", "CAGCGC", "TTCTGT", "ATAAGG", "CTCCTA", "TTACTG"],
        ["ATGCGA", "CTGCGC", "TTCTCA", "ATAAGA", "CGCCTA", "TTACTA"],
        ["ATTTTA", "CTGCGC", "TTCTCC", "ATAAGA", "CGCCTA", "TTACTA"],
        ["ATTTTA", "CTGCGC", "TTCTCC", "ATAAGA", "CAAAAA", "TTACTA"],
    ]

    listDnaWithMutation.forEach(dna => {
        it(`check dna with mutation - ${dna} `, async () => {
            const response = await mutationRequest(dna)
            expect(response.statusCode).toEqual(200)
            expect(response.body).toBeTruthy()
        })
    })

    const listDnaWithoutMutation = [
        ["ATGCGA","CAGTGC","TTATTT","AGACGG","GCGTCA","TCACTG"],
        ["ATGCGA", "CAGGCC", "TTATGT", "AGAAGG", "CTCCTA", "TCACTG"],
    ]

    listDnaWithoutMutation.forEach(dna => {
        it(`check dna without mutation - ${dna} `, async () => {
            const response = await mutationRequest(dna)
            expect(response.statusCode).toEqual(403)
            expect(response.body).toBeFalsy()
        })
    })

    it('get stats', async () => {
        const response = await request(app)
            .get('/api/stats')

        expect(response.statusCode).toEqual(200)
        expect(response.body).toEqual({
            count_mutations: listDnaWithMutation.length, 
            count_no_mutation: listDnaWithoutMutation.length, 
            ratio: (listDnaWithMutation.length / listDnaWithoutMutation.length).toFixed(2)
        })
    })

    it('check dna validation null', async () => {
        const response = await mutationRequest(null)
        expect(response.statusCode).toEqual(400)
    })

    it('check dna validation empty', async () => {
        const response = await mutationRequest([])
        expect(response.statusCode).toEqual(400)
    })

    it('check dna validation format', async () => {
        const response = await mutationRequest(['ABCDEF'])
        expect(response.statusCode).toEqual(400)
    })
})