
const app  = require('./src/app')

const port = app.settings.config.server.port

const server = app.listen(port, () => {
    console.log(`Server is listening on port: ${server.address().port}`)
})
