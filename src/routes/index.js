const router = require('express').Router();
const BASE_URL = '/api';

// CONTROLLERS
const homeController = require('../controllers/home.controller');

// ROUTES
router.get(BASE_URL + '/', homeController.index);
router.post(BASE_URL + '/mutation', homeController.mutation);
router.get(BASE_URL + '/stats', homeController.stats);

module.exports = router;