const express = require('express')
const routes = require('./routes')
const { setupDatabase } = require('./database')

const app = express()
setupDatabase()

app.set('config', require('./config/app'))
app.use(express.urlencoded({ extended: true }))
app.use(express.json())
app.use('/', routes)

module.exports = app