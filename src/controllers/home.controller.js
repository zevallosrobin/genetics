const { isMutation, dnaValidation} = require('./mutation')
const dnaVerifiedRepository = require('../database/repository/dnaVerified.repository')

class HomeController {
    index(req, res) {
        res.send('genetics api')
    }
    
    async mutation(req, res, next) {
        try {
            const { dna } = req.body

            if (!dnaValidation(dna)) {
                res.status(400).send({ errorMessage: 'dna format is not correct.'})
                return next()
            }

            const _isMutation = isMutation(dna)

            await dnaVerifiedRepository.createUnique({ 
                dna: dna.join('-'), 
                isMutation: _isMutation
            })

            if (_isMutation) {
                res.status(200).send(true)
            } else {
                res.status(403).send(false)
            }
    
        } catch (error) {
            console.log(error)
            res.status(500)
        }
    }

    async stats(req, res) {
        try {
            const all = await dnaVerifiedRepository.getAll()
            const countMutations = all.filter(value => value.isMutation).length
            const countNoMutations = all.length - countMutations

            res.send({
                count_mutations: countMutations, 
                count_no_mutation:countNoMutations, 
                ratio: countNoMutations == 0 ? 0 : (countMutations / countNoMutations).toFixed(2)
            })
        } catch (error) {
            console.log(error)
            res.status(500)
        }
    }
};

module.exports = new HomeController()