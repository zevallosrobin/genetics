function allSequencesMode(dna) {
    const lengthCombinations = dna.length * 2 + 2
    const result = Array(lengthCombinations).fill('')

    dna.forEach((sequence, indexSequence) => {
        // horizontal
        result[indexSequence] = sequence

        //vertical
        Array.from(sequence).forEach((letter, indexLetter) => {
            result[sequence.length + indexLetter] += letter
        })

        //diagonal
        result[result.length - 2] += sequence[indexSequence]
        result[result.length - 1] += sequence[sequence.length - 1 - indexSequence]
    })

    return result
}

function isMutation(dna) {
    const allSequences = allSequencesMode(dna)

    let sequenceRepeatedCount = 0

    allSequences.forEach(sequence => {
        const isSequenceRepeated = [sequence.substr(0, 4), sequence.substr(1, 4), sequence.substr(2, 4)]
            .some(value => Array.from(value).every(c => c == value[0]))
        
        if (isSequenceRepeated) {
            sequenceRepeatedCount++
        }
    })

    return sequenceRepeatedCount >= 2
}

function dnaValidation(dna) {
    try {
        if (!dna) return false
        if (dna.length == 0) return false
        if (dna.some(sequence => sequence.length != dna.length )) return false
    
        const nitrogenBase = 'ATCG'
        const isNitrogenBase = dna.every(sequence => Array.from(sequence).every(character => nitrogenBase.includes(character)))
    
        return isNitrogenBase
    } catch {
        return false
    }
}

module.exports = { isMutation, dnaValidation }