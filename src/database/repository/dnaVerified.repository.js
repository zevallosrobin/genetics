const DnaVerified = require('../entities/dnaVerified.entity')

class DnaVerifiedRepository {
    async getAll() {
        const result = (await DnaVerified.findAll()).map(value => value.toJSON())
    
        return result
    }
    
    async getByDna(dna) {
        const result = (await DnaVerified.findAll({ where: {dna: dna}} )).map(value => value.toJSON())
    
        return result
    }
    
    async createUnique({ dna, isMutation }) {
        const existsDna = (await this.getByDna(dna)).length >= 1
        if (existsDna) return

        const result = await DnaVerified.create({ dna, isMutation })
    
        return result.toJSON()
    }
}

module.exports = new DnaVerifiedRepository()

