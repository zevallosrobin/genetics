
const { Sequelize, DataTypes } = require('sequelize')
const DnaVerified = require('./entities/dnaVerified.entity')

async function setupDatabase() {
    const sequelize = new Sequelize('sqlite::memory:', {
        logging: false
    })

    DnaVerified.init({
        dna: DataTypes.STRING,
        isMutation: DataTypes.BOOLEAN
    }, { sequelize, modelName: 'dnaVerified' })

    await sequelize.sync()
}

module.exports = { setupDatabase }