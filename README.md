# [Genetics API](https://genetics-1001.herokuapp.com/api) 
Service that detects if a person has genetic differences based on their DNA sequence

# Setup

## 1. Install dependencies:

```npm install```

## 2. Run development server:

```npm start```


Open your browser and enter [localhost:9001/api](http://localhost:9001/api) into the address bar.

## 3. Run test:

```npm run test```

## Production web server:

Open your browser and enter [https://genetics-1001.herokuapp.com/api](https://genetics-1001.herokuapp.com/api) into the address bar.

## Endpoints

```/mutation```

```/stats```

